package edu.westga.cs1302.grades.viewmodel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import edu.westga.cs1302.grades.model.GradeBook;
import edu.westga.cs1302.grades.model.Student;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * This class creates a object to be used in a view of an application.
 * 
 * @author jeremy.trimble
 * @version 7/18/2018
 */
public class GradesViewModel {
	private StringProperty about;
	private StringProperty studentName;
	private FileChooser fileChooser;
	private File gradesFile;
	private GradeBook grades;
	private StringProperty studentID;
	private IntegerProperty labAverage;
	private IntegerProperty testAverage;
	private IntegerProperty projectAverage;
	private IntegerProperty overallAverage;
	private IntegerProperty weightedAverage;
	private FileWriter fileWriter;

	/**
	 * The constructor of the viewmodel object
	 * 
	 * @precondition: none
	 * @postcondition: new viewmodel object is created
	 * 
	 */
	public GradesViewModel() {
		this.about = new SimpleStringProperty();
		this.about.setValue(makeHelpString());
		this.fileChooser = new FileChooser();
		this.studentName = new SimpleStringProperty();
		this.studentID = new SimpleStringProperty();
		this.labAverage = new SimpleIntegerProperty();
		this.testAverage = new SimpleIntegerProperty();
		this.projectAverage = new SimpleIntegerProperty();
		this.overallAverage = new SimpleIntegerProperty();
		this.weightedAverage = new SimpleIntegerProperty();
		this.studentName.set("");
		this.studentID.set("");

	}

	/**
	 * Helper method to define the string in the helpString
	 * 
	 * @precondition: none
	 * @postcondition: helpString is initialized
	 */
	private String makeHelpString() {
		return "Created by: Jeremy Trimble\nThis program displays grade information about students.";
	}

	/**
	 * Returns the String property for the about dialog
	 * 
	 * @precondition: none
	 * @return The string property for the about information
	 */
	public StringProperty getAbout() {
		return this.about;
	}

	/**
	 * Sets the file to be opened
	 * 
	 * @precondition: none
	 * 
	 * @postcondition: this.gradesFile is set
	 */
	public void setGradeFile() {
		try {
			this.fileChooser.setTitle("Choose a File to Open");
			Stage fileStage = new Stage();
			this.fileChooser.getExtensionFilters().add(new ExtensionFilter("Text Files", "*.txt"));
			this.gradesFile = this.fileChooser.showOpenDialog(fileStage);
			this.grades = new GradeBook(this.gradesFile);
			this.grades.loadStudentsFromFile();
			this.grades.loadAllGrades();
		} catch (Exception exception) {
			Alert badFile = new Alert(Alert.AlertType.ERROR);
			badFile.setTitle("Bad File");
			badFile.setContentText(
					"File's data is inconsistent with required format.\nPlease review the file's content or try a different file.");
			badFile.showAndWait();
		}

	}

	/**
	 * Saves the all the grades information to a file
	 * 
	 * @precondition none
	 * @postcondition new file is saved
	 */
	public void saveAllGrades() {

		try {
			this.fileChooser.setTitle("Save As");
			Stage saveStage = new Stage();
			File newFile = this.fileChooser.showSaveDialog(saveStage);

			this.fileWriter = new FileWriter(newFile);
			this.fileWriter.write(this.grades.allStudentsGrades());
			this.fileWriter.close();

		} catch (IOException ioe) {
			Alert ioException = new Alert(Alert.AlertType.ERROR);
			ioException.setTitle("IO Exception");
			ioException.setContentText("An error occured and your file may not have been saved.");
			ioException.showAndWait();
		} catch (NullPointerException npe) {
			Alert nullpointer = new Alert(Alert.AlertType.ERROR);
			nullpointer.setTitle("No file to write");
			nullpointer.setContentText("An error occured and your file may not have been saved.");
			nullpointer.showAndWait();
		}
	}

	/**
	 * Saves the displayed information to a file
	 * 
	 * @precondition none
	 * @postcondition new file is saved
	 */
	public void saveDisplayedGrades() {

		try {
			this.fileChooser.setTitle("Save As");
			Stage saveStage = new Stage();
			File newFile = this.fileChooser.showSaveDialog(saveStage);
			Student currentStudent = this.grades.getStudent(this.studentID.getValue());

			this.fileWriter = new FileWriter(newFile);
			this.fileWriter.write(this.grades.currentStudentsGrades(currentStudent));
			this.fileWriter.close();

		} catch (IOException ioe) {
			Alert ioException = new Alert(Alert.AlertType.ERROR);
			ioException.setTitle("IO Exception");
			ioException.setContentText("An error occured and your file may not have been saved.");
			ioException.showAndWait();
		} catch (NullPointerException npe) {
			Alert nullpointer = new Alert(Alert.AlertType.ERROR);
			nullpointer.setTitle("No file to write");
			nullpointer.setContentText("An error occured and your file may not have been saved.");
			nullpointer.showAndWait();
		}
	}

	/**
	 * Sets all the properties based on the student provided
	 * 
	 * @precondition none
	 * @postcondition all properties are set
	 * 
	 */
	public void selectStudent() {
		try {
			String currentID = this.studentID.getValue();
			Student selected = this.grades.getStudent(currentID);
			this.studentName.set(selected.getFullName());
			this.labAverage.set(this.grades.getStudent(currentID).getLabsAverage());
			this.testAverage.set(this.grades.getStudent(currentID).getTestsAverage());
			this.projectAverage.set(this.grades.getStudent(currentID).getProjectsAverage());
			this.overallAverage.set(this.grades.getStudent(currentID).getOverallAverage());
			this.weightedAverage.set(this.grades.getStudent(currentID).getWeightedAverage());
		} catch (NullPointerException npe) {
			Alert nullpointer = new Alert(Alert.AlertType.ERROR);
			nullpointer.setTitle("ID is invalid");
			nullpointer.setContentText(
					"The ID you entered does not exist in current file.\nCheck you have the correct file and ID number");
			nullpointer.showAndWait();
		}

	}

	/**
	 * Returns the student's name
	 * 
	 * @precondition none
	 * 
	 * @return the current student's name
	 */
	public StringProperty getStudentName() {
		return this.studentName;
	}

	/**
	 * Returns the student's Lab Average
	 * 
	 * @precondition none
	 * @return the average of the student's lab grades
	 */
	public IntegerProperty getStudentLabAverage() {
		return this.labAverage;

	}

	/**
	 * Returns the student's Project Average
	 * 
	 * @precondition none
	 * @return the average of the student's project grades
	 */
	public IntegerProperty getStudentProjectAverage() {
		return this.projectAverage;
	}

	/**
	 * Returns the student's Test Average
	 * 
	 * @precondition none
	 * @return the average of the student's lab grades
	 * 
	 * 
	 */
	public IntegerProperty getStudentTestAverage() {
		return this.testAverage;
	}

	/**
	 * Returns the student's overall Average
	 * 
	 * @precondition none
	 * @return the average of all the student's grades
	 * 
	 * 
	 */
	public IntegerProperty getStudentOverallAverage() {
		return this.overallAverage;
	}

	/**
	 * Returns the student's Weighted Average
	 * 
	 * @precondition none
	 * @return the weight average for the student
	 */
	public IntegerProperty getStudentWeightedAverage() {
		return this.weightedAverage;
	}

	/**
	 * Returns the student's ID StringProperty
	 * 
	 * @precondition none
	 * @return the student's ID
	 */
	public StringProperty getStudentID() {
		return this.studentID;
	}

	/**
	 * Updates the graph with the values for the current student
	 * 
	 * @precondition none
	 * @return the series to be entered in the chart
	 */
	public XYChart.Series<String, Integer> updateChart() {
		Integer labs = this.labAverage.getValue();
		Integer tests = this.testAverage.getValue();
		Integer projects = this.projectAverage.getValue();
		Series<String, Integer> series1 = new Series<String, Integer>();
		series1.setName("Grades for " + this.studentName.getValue());
		series1.getData().add(new XYChart.Data<String, Integer>("Labs", labs));
		series1.getData().add(new XYChart.Data<String, Integer>("Projects", projects));
		series1.getData().add(new XYChart.Data<String, Integer>("Tests", tests));
		return series1;
	}

	/**
	 * Setups emtpy data for chart so the catergory labels aren't stacked on first
	 * call
	 * 
	 * @precondition none
	 * @return an empty series with labels
	 */
	public XYChart.Series<String, Integer> fillEmptyChart() {
		Series<String, Integer> series0 = new Series<String, Integer>();
		series0.setName("No Student Selected");
		series0.getData().add(new XYChart.Data<String, Integer>("Labs", 0));
		series0.getData().add(new XYChart.Data<String, Integer>("Projects", 0));
		series0.getData().add(new XYChart.Data<String, Integer>("Tests", 0));
		return series0;
	}

	/**
	 * Adds a point to Test03
	 * 
	 * @precondition none
	 * @postcondition Test03 has another point
	 */
	public void addPointToTestThree() {
		try {
			this.grades.addPointToTestThreeEachStudent();
		} catch (IllegalArgumentException iae) {
			Alert noTestThree = new Alert(Alert.AlertType.ERROR);
			noTestThree.setTitle("Test Not found");
			noTestThree.setContentText("No Test03 found in file.\nPlease select another file.");
			noTestThree.showAndWait();
		} catch (NullPointerException npe) {
			Alert noStudent = new Alert(Alert.AlertType.ERROR);
			noStudent.setTitle("No Student Selected");
			noStudent.setContentText("No student has been selected yet.\nPlease select a student and try again.");
			noStudent.showAndWait();
		}
	}

}
