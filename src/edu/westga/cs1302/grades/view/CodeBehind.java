package edu.westga.cs1302.grades.view;

import edu.westga.cs1302.grades.viewmodel.GradesViewModel;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;

/**
 * This is the code behind the view that controls results of actions to the
 * interface.
 * 
 * @author jeremy.trimble
 * @version 7/18/2018
 */
public class CodeBehind {
	@FXML
	private AnchorPane gradeBook;

	@FXML
	private Label lblTitle;

	@FXML
	private Label lblStudentName;

	@FXML
	private Label lblLab;

	@FXML
	private Label lblOverAll;

	@FXML
	private Label lblTest;

	@FXML
	private Label lblProject;

	@FXML
	private Label lblOutputName;

	@FXML
	private Label lblOutputLab;

	@FXML
	private Label lblOutputProject;

	@FXML
	private Label lblOutputTest;

	@FXML
	private Label lblOutputOverall;

	@FXML
	private Menu barFile;

	@FXML
	private MenuItem fileOpen;

	@FXML
	private MenuItem fileSave;

	@FXML
	private Menu barHelp;

	@FXML
	private TextField txtSearchStudent;

	@FXML
	private Button btnStudentSearch;

	@FXML
	private Label lblWeightAverage;

	@FXML
	private Label lblOutputWeighted;

	@FXML
	private BarChart<String, Integer> barGraph;

	@FXML
	private CategoryAxis barHAxis;

	@FXML
	private NumberAxis barVAxis;

	@FXML
	private Button btnAddPoint;

	@FXML
	private MenuItem helpAbout;

	@FXML
	private GradesViewModel grades;

	/**
	 * Creates the CodeBehind object
	 * 
	 * @precondition: none
	 *
	 */
	public CodeBehind() {
		this.grades = new GradesViewModel();

	}

	/**
	 * Binds the fxml objects to the viewmodel object
	 * 
	 * @precondition: none
	 * @postcondition: none
	 */
	@FXML
	public void initialize() {
		this.lblOutputName.textProperty().bindBidirectional(this.grades.getStudentName());
		this.txtSearchStudent.textProperty().bindBidirectional(this.grades.getStudentID());
		this.lblOutputLab.textProperty().bindBidirectional(this.grades.getStudentLabAverage(),
				new NumberStringConverter());
		this.lblOutputProject.textProperty().bindBidirectional(this.grades.getStudentProjectAverage(),
				new NumberStringConverter());
		this.lblOutputTest.textProperty().bindBidirectional(this.grades.getStudentTestAverage(),
				new NumberStringConverter());
		this.lblOutputOverall.textProperty().bindBidirectional(this.grades.getStudentOverallAverage(),
				new NumberStringConverter());
		this.lblOutputWeighted.textProperty().bindBidirectional(this.grades.getStudentWeightedAverage(),
				new NumberStringConverter());
		this.barGraph.layoutXProperty().bindBidirectional(this.gradeBook.layoutXProperty());
		this.barGraph.getData().add(this.grades.fillEmptyChart());

	}

	/**
	 * Displays the about information in the help menu
	 * 
	 * @precondition: none
	 * @postcondition: Dialog box is displayed
	 * 
	 */
	public void getAbout() {
		Alert aboutDialog = new Alert(AlertType.INFORMATION);
		aboutDialog.setHeaderText("About Gradebook");
		aboutDialog.setTitle("About Gradebook");
		aboutDialog.setResizable(true);
		aboutDialog.contentTextProperty().bindBidirectional(this.grades.getAbout());
		aboutDialog.showAndWait();
	}

	/**
	 * Opens the fileChooser dialog
	 * 
	 * @precondition none
	 * @postcondition File is selected
	 */
	public void setGradesFile() {
		this.grades.setGradeFile();

	}

	/**
	 * Selects the student typed in the Student ID field
	 * 
	 * @precondition none
	 * @postcondition student information changes to current student
	 */
	public void selectStudent() {
		this.grades.selectStudent();
		this.updateChart();

	}

	/**
	 * Updates the chart
	 * 
	 * @precondition none
	 * @postcondition chart displays student's information
	 */
	public void updateChart() {
		this.barGraph.getData().removeAll(this.barGraph.getData());
		this.barGraph.getData().add(this.grades.updateChart());
	}

	/**
	 * Adds a point to test three grade
	 * 
	 * @precondition none
	 * @postcondition test three is one point higher
	 */
	public void addPointToTestThree() {
		this.grades.addPointToTestThree();
		this.selectStudent();
	}

	/**
	 * Saves the current displayed student
	 * 
	 * @precondition none
	 * @postcondition new file is saved
	 */
	public void saveDisplayedGrades() {
		this.grades.saveDisplayedGrades();
	}

}
