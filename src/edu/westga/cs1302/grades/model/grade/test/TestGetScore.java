package edu.westga.cs1302.grades.model.grade.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;

/**
 * Test the get Score method
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestGetScore {

	/**
	 * Test max points value is less than 100
	 */
	@Test
	public void testMaxPointsLessThanOneHundred() {
		Grade theGrade = new Grade(50, 45);

		assertEquals(90, theGrade.getScore());
	}

	/**
	 * Test max points value is more than 100
	 */
	@Test
	public void testMaxPointsMoreThanOneHundred() {
		Grade theGrade = new Grade(105, 100);

		assertEquals(95, theGrade.getScore());
	}

	/**
	 * Test max points value is at 100
	 */
	@Test
	public void testMaxPointsAtOneHundred() {
		Grade theGrade = new Grade(100, 70);

		assertEquals(70, theGrade.getScore());
	}

	/**
	 * Test grade rounding up
	 */
	@Test
	public void testRoundUp() {
		Grade theGrade = new Grade(70, 53);

		assertEquals(76, theGrade.getScore());
	}

}
