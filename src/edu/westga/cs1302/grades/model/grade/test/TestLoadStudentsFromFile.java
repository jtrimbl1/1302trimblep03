package edu.westga.cs1302.grades.model.grade.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.GradeBook;

/**
 * Test the read students method
 * 
 * @author jeremy.trimble
 * @version 7/19/2018
 */
public class TestLoadStudentsFromFile {

	/**
	 * Test adding students from a file
	 * 
	 * @precondition: none
	 */
	@Test
	public void testLoadingStudents() {
		File newFile = new File("../1302TrimbleProject3/GradesForTesting");
		GradeBook grades = new GradeBook(newFile);
		grades.loadStudentsFromFile();
		String result = grades.getStudents().get(0).toString();

		assertEquals("1234 is the ID Number for Joe Student.", result);
	}

}
