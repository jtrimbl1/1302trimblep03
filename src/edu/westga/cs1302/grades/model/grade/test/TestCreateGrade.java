package edu.westga.cs1302.grades.model.grade.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;

/**
 * Test the creation of a new grade object
 * 
 * @author jeremy.trimble
 * @version 7/16/2018
 */
public class TestCreateGrade {

	/**
	 * Test creating a new grade
	 * 
	 * @precondition: none
	 * 
	 */
	@Test
	public void testCreateGrade() {
		Grade newGrade = new Grade(100, 100);

		assertEquals("100 out of 100", newGrade.toString());
	}

}
