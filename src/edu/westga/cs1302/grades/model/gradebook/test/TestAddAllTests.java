package edu.westga.cs1302.grades.model.gradebook.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.GradeBook;

/**
 * Test adding all test to the students
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestAddAllTests {

	/**
	 * Test adding all the tests to the student
	 * 
	 * @precondition none
	 */
	@Test
	public void testAddAllTests() {
		File newFile = new File("../1302TrimbleProject3/GradesForTesting");
		GradeBook grades = new GradeBook(newFile);
		grades.loadStudentsFromFile();
		grades.addAllTests();

		assertEquals(2, grades.getStudents().get(0).countOfTests());
	}

}
