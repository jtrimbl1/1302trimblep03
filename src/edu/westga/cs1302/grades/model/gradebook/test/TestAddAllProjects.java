package edu.westga.cs1302.grades.model.gradebook.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.GradeBook;

/**
 * Test adding all the projects for students
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestAddAllProjects {

	/**
	 * Test adding all the projects to the student
	 * 
	 * @precondition none
	 */
	@Test
	public void testAddAllProjects() {
		File newFile = new File("../1302TrimbleProject3/GradesForTesting");
		GradeBook grades = new GradeBook(newFile);
		grades.loadStudentsFromFile();
		grades.addAllProjects();

		assertEquals(2, grades.getStudents().get(0).countOfProjects());
	}

}
