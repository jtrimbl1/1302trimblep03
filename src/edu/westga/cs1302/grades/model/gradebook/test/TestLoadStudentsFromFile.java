package edu.westga.cs1302.grades.model.gradebook.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.GradeBook;

/**
 * Test the method used to create and add students to gradebook object
 * 
 * @author jeremy.trimble
 * @version 7/20/2018
 */
class TestLoadStudentsFromFile {

	/**
	 * test loading one students from file
	 */
	@Test
	void testLoadStudentFromFile() {
		File newFile = new File("../1302TrimbleProject3/GradesForTesting");
		GradeBook grades = new GradeBook(newFile);
		grades.loadStudentsFromFile();

		assertEquals(1, grades.getStudents().size());
	}

	/**
	 * Test Loading more than one Student from File
	 * 
	 * @precondition none
	 */
	@Test
	void testLoadMulitpleStudentsFromFile() {
		File newFile = new File("../1302TrimbleProject3/SampleFromMoodle");
		GradeBook grades = new GradeBook(newFile);
		grades.loadStudentsFromFile();

		assertEquals(3, grades.getStudents().size());
	}

}
