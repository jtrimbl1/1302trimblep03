package edu.westga.cs1302.grades.model.gradebook.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.GradeBook;

/**
 * Test the creation of a GradeBook Object
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestCreateGradeBook {

	/**
	 * Test creating a new GradeBook
	 */
	@Test
	public void testCreateGradeBook() {
		File theFile = new File("../1302TrimbleProject3/GradesForTesting");
		GradeBook theBookOfGrades = new GradeBook(theFile);
		theBookOfGrades.loadStudentsFromFile();

		assertEquals("This gradebook holds 1 students.", theBookOfGrades.toString());

	}

}
