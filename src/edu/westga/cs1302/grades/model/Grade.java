package edu.westga.cs1302.grades.model;

/**
 * Manages Grade objects
 * 
 * @author jeremy.trimble
 * @version 7/16/2018
 */
public class Grade {
	private int maxPoints;
	private int pointsEarned;

	/**
	 * Creates a new grade with the given max points and points earned
	 * 
	 * 
	 * @precondition: maxPoints >= 0 && pointsEarned >= 0
	 * @postcondition: new grade object is created
	 * 
	 * @param maxPoints
	 *            The highest score possible
	 * @param pointsEarned
	 *            The score for this grade
	 */
	public Grade(int maxPoints, int pointsEarned) {
		if (this.maxPoints < 0) {
			throw new IllegalArgumentException("Max points must be 0 or greater than 0");
		}

		if (this.pointsEarned < 0) {
			throw new IllegalArgumentException("Points earned must be 0 or greater");
		}
		this.maxPoints = maxPoints;
		this.pointsEarned = pointsEarned;
	}

	/**
	 * Overrides toString with a description of the grade object
	 * 
	 * @precondition: none
	 * 
	 * @return A string describing the object
	 * 
	 */
	public String toString() {
		return this.pointsEarned + " out of " + this.maxPoints;
	}

	/**
	 * Returns the score for the grade
	 * 
	 * @precondition none
	 * @return the score for the grade
	 */
	public int getScore() {
		double earned = this.pointsEarned;
		double max = this.maxPoints;
		return (int) ((((earned / max) + 0.005) * 100));
	}

	/**
	 * Adds a point to the grade
	 * 
	 * @precondition none
	 * @postcondition pointsEarned = pointEarned@prev + 1
	 */
	public void addPoint() {
		this.pointsEarned++;
	}

	/**
	 * Returns the pointsEarned
	 * 
	 * @precondition
	 * @return the pointsEarned
	 */
	public int getPointsEarned() {
		return this.pointsEarned;
	}

	/**
	 * Returns the maxPoints
	 * 
	 * @precondition
	 * @return the maxPoints
	 */
	public int getMaxPoints() {
		return this.maxPoints;
	}

}
