package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test getting the weighted average for the student
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestGetWeightedAverage {
	private Student student1;

	/**
	 * Runs before each test
	 * 
	 * @precondition none
	 */
	@BeforeEach
	public void setUp() {
		Grade lab1 = new Grade(10, 9);
		Grade lab2 = new Grade(10, 8);
		Grade project1 = new Grade(100, 95);
		Grade project2 = new Grade(100, 90);
		Grade test1 = new Grade(100, 89);
		Grade test2 = new Grade(100, 85);
		this.student1 = new Student("5", "Reese", "Trip");
		this.student1.addGradeToLabs(lab1);
		this.student1.addGradeToLabs(lab2);
		this.student1.addGradeToProjects(project1);
		this.student1.addGradeToProjects(project2);
		this.student1.addGradeToTests(test1);
		this.student1.addGradeToTests(test2);
	}

	/**
	 * Test rounding Up to the nearest integer
	 * 
	 * @precondition none
	 */
	@Test
	public void testRoundUp() {
		assertEquals(89, this.student1.getWeightedAverage());
	}

	/**
	 * Test rounding down to the nearest integer
	 * 
	 * @precondition none
	 */
	@Test
	public void testRoundDown() {
		Grade lab3 = new Grade(100, 75);
		this.student1.addGradeToLabs(lab3);

		assertEquals(88, this.student1.getWeightedAverage());
	}

	/**
	 * Test no rounding needed
	 * 
	 * @precondition none
	 */
	@Test
	public void testNoRounding() {
		Grade lab3 = new Grade(100, 75);
		Grade project3 = new Grade(100, 75);
		this.student1.addGradeToLabs(lab3);
		this.student1.addGradeToProjects(project3);

		assertEquals(86, this.student1.getWeightedAverage());
	}

}
