package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test adding different types of grades to a student object
 * 
 * @author jeremy.trimble
 * @version 7/19/2018
 */
class TestAddGrade {
	private Student tom;

	/**
	 * Setup before each test
	 * 
	 * @precondition none
	 * 
	 */
	@BeforeEach
	void setUp() {
		this.tom = new Student("123", "Tom", "Sawyer");

	}

	@Test
	void testAddLab() {
		Grade lab1 = new Grade(10, 10);
		this.tom.addGradeToLabs(lab1);

		assertEquals(1, this.tom.countOfLabs());
	}

	@Test
	void testAddTwoLabs() {
		Grade lab1 = new Grade(10, 10);
		Grade lab2 = new Grade(10, 9);
		this.tom.addGradeToLabs(lab1);
		this.tom.addGradeToLabs(lab2);

		assertEquals(2, this.tom.countOfLabs());
	}

	@Test
	void testAddTest() {
		Grade test1 = new Grade(10, 10);
		this.tom.addGradeToTests(test1);

		assertEquals(1, this.tom.countOfTests());
	}

	@Test
	void testAddTwoTests() {
		Grade test1 = new Grade(10, 10);
		Grade test2 = new Grade(10, 8);
		this.tom.addGradeToLabs(test1);
		this.tom.addGradeToLabs(test2);

		assertEquals(2, this.tom.countOfLabs());
	}

	@Test
	void testAddProject() {
		Grade project1 = new Grade(100, 95);
		this.tom.addGradeToLabs(project1);

		assertEquals(1, this.tom.countOfLabs());
	}

	@Test
	void testAddTwoProjects() {
		Grade project1 = new Grade(100, 95);
		Grade project2 = new Grade(100, 95);
		this.tom.addGradeToLabs(project1);
		this.tom.addGradeToLabs(project2);

		assertEquals(2, this.tom.countOfLabs());
	}

}
