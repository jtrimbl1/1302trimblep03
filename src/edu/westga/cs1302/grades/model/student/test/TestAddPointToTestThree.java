package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test adding a point to test03
 * 
 * @author jeremy.trimble
 * @version 7/22/2018
 */
public class TestAddPointToTestThree {

	/**
	 * Test adding two point to increase test average by one point
	 * 
	 * @precondition none
	 */
	@Test
	public void testAddPointToTestThree() {
		Grade test1 = new Grade(100, 100);
		Grade test2 = new Grade(100, 95);
		Grade test3 = new Grade(100, 90);
		Student student1 = new Student("123", "Jim", "Jackson");
		student1.addGradeToTests(test1);
		student1.addGradeToTests(test2);
		student1.addGradeToTests(test3);
		student1.addPointToTestThree();
		student1.addPointToTestThree();

		assertEquals(96, student1.getTestsAverage());
	}

}
