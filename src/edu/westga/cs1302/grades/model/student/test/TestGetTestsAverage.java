package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

class TestGetTestsAverage {
	/**
	 * Result of average is rounded up.
	 * 
	 * @precondition none
	 */
	@Test
	public void testResultNeedsRoundedUp() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade test1 = new Grade(10, 10);
		Grade test2 = new Grade(10, 9);
		Grade test3 = new Grade(10, 8);
		Grade test4 = new Grade(10, 7);
		student1.addGradeToTests(test1);
		student1.addGradeToTests(test2);
		student1.addGradeToTests(test3);
		student1.addGradeToTests(test4);

		assertEquals(85, student1.getTestsAverage());
	}

	/**
	 * Result is a whole number that isn't required to be rounded
	 * 
	 * @precondition none
	 */
	@Test
	public void testResultIsWholeNumber() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade test1 = new Grade(10, 10);
		Grade test2 = new Grade(10, 9);
		Grade test3 = new Grade(10, 8);
		student1.addGradeToTests(test1);
		student1.addGradeToTests(test2);
		student1.addGradeToTests(test3);

		assertEquals(90, student1.getTestsAverage());
	}

	/**
	 * Result is rounded down
	 */
	@Test
	public void testResultNeedsRoundedDown() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade test1 = new Grade(10, 5);
		Grade test2 = new Grade(10, 5);
		Grade test3 = new Grade(10, 3);
		student1.addGradeToTests(test1);
		student1.addGradeToTests(test2);
		student1.addGradeToTests(test3);

		assertEquals(43, student1.getTestsAverage());
	}

}
