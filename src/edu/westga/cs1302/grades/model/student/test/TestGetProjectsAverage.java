package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test getting the average of projects
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestGetProjectsAverage {
	/**
	 * Result of average is rounded up.
	 * 
	 * @precondition none
	 */
	@Test
	public void testResultNeedsRoundedUp() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade project1 = new Grade(10, 10);
		Grade project2 = new Grade(10, 9);
		Grade project3 = new Grade(10, 8);
		Grade project4 = new Grade(10, 7);
		student1.addGradeToLabs(project1);
		student1.addGradeToLabs(project2);
		student1.addGradeToLabs(project3);
		student1.addGradeToLabs(project4);

		assertEquals(85, student1.getLabsAverage());
	}

	/**
	 * Result is a whole number that isn't required to be rounded
	 * 
	 * @precondition none
	 */
	@Test
	public void testResultIsWholeNumber() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade project1 = new Grade(10, 10);
		Grade project2 = new Grade(10, 9);
		Grade project3 = new Grade(10, 8);
		student1.addGradeToLabs(project1);
		student1.addGradeToLabs(project2);
		student1.addGradeToLabs(project3);

		assertEquals(90, student1.getLabsAverage());
	}

	/**
	 * Result is rounded down
	 */
	@Test
	public void testResultNeedsRoundedDown() {
		Student student1 = new Student("213", "Jerry", "Spoon");
		Grade project1 = new Grade(10, 5);
		Grade project2 = new Grade(10, 5);
		Grade project3 = new Grade(10, 3);
		student1.addGradeToLabs(project1);
		student1.addGradeToLabs(project2);
		student1.addGradeToLabs(project3);

		assertEquals(43, student1.getLabsAverage());
	}

}
