package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test getting the overall average of all Grades for the student
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestGetOverallAverage {

	/**
	 * Test getting the overall average for all the grades the student has
	 * 
	 * @precondition none
	 */
	@Test
	public void testGetOverallAverage() {
		Student kyle = new Student("8", "Kyle", "Roberts");
		Grade lab1 = new Grade(10, 8);
		Grade lab2 = new Grade(10, 9);
		Grade project1 = new Grade(100, 90);
		Grade project2 = new Grade(100, 90);
		Grade test1 = new Grade(100, 70);

		kyle.addGradeToLabs(lab1);
		kyle.addGradeToLabs(lab2);
		kyle.addGradeToProjects(project1);
		kyle.addGradeToProjects(project2);
		kyle.addGradeToTests(test1);

		assertEquals(84, kyle.getOverallAverage());

	}

}
