package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test getting the count of test, projects, and labs
 * 
 * @author jeremy.trimble
 * @version 7/21/2018
 */
public class TestCountOfs {

	/**
	 * Test the countOfLabs
	 */
	@Test
	public void testCountOfLabs() {
		Grade grade1 = new Grade(10, 10);
		Grade grade2 = new Grade(10, 9);
		Grade grade3 = new Grade(10, 8);

		Student newStudent = new Student("213", "Larry", "Howard");
		newStudent.addGradeToLabs(grade1);
		newStudent.addGradeToLabs(grade2);
		newStudent.addGradeToLabs(grade3);

		assertEquals(3, newStudent.countOfLabs());
	}

	/**
	 * Test the countOfTest
	 */
	@Test
	public void testCountOfTests() {
		Grade grade1 = new Grade(10, 10);
		Grade grade2 = new Grade(10, 9);

		Student newStudent = new Student("213", "Larry", "Howard");
		newStudent.addGradeToTests(grade1);
		newStudent.addGradeToTests(grade2);

		assertEquals(2, newStudent.countOfTests());
	}

	/**
	 * Test the countOfProjects
	 */
	@Test
	public void testCountOfProjects() {
		Grade grade1 = new Grade(10, 10);
		Grade grade2 = new Grade(10, 9);
		Grade grade3 = new Grade(10, 8);

		Student newStudent = new Student("213", "Larry", "Howard");
		newStudent.addGradeToProjects(grade1);
		newStudent.addGradeToProjects(grade2);
		newStudent.addGradeToProjects(grade3);

		assertEquals(3, newStudent.countOfProjects());
	}

}
