package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Student;

/**
 * Test creating a student object
 * 
 * @author jeremy.trimble
 * @version 7/16/2018
 */
public class TestCreateStudent {

	/**
	 * Test the creation of a student
	 * 
	 * @precondition: none
	 * 
	 */
	@Test
	public void testCreateStudent() {
		Student newStudent = new Student("12", "Fred", "Thomas");

		assertEquals("12 is the ID Number for Fred Thomas.", newStudent.toString());
	}

}
