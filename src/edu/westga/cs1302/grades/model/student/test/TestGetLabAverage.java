package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test calculating the average of the lab scores
 * 
 * @author jeremy.trimble
 * @version 7/20/2018
 */
class TestGetLabAverage {

	@Test
	void testResultNeedsRoundedUp() {
		Student student1 = new Student("213", "Tom", "Thomas");
		Grade lab1 = new Grade(10, 10);
		Grade lab2 = new Grade(10, 9);
		Grade lab3 = new Grade(10, 8);
		Grade lab4 = new Grade(10, 7);
		student1.addGradeToLabs(lab1);
		student1.addGradeToLabs(lab2);
		student1.addGradeToLabs(lab3);
		student1.addGradeToLabs(lab4);

		assertEquals(85, student1.getLabsAverage());
	}

	@Test
	void testResultIsWholeNumber() {
		Student student1 = new Student("213", "Tom", "Thomas");
		Grade lab1 = new Grade(10, 10);
		Grade lab2 = new Grade(10, 9);
		Grade lab3 = new Grade(10, 8);
		student1.addGradeToLabs(lab1);
		student1.addGradeToLabs(lab2);
		student1.addGradeToLabs(lab3);

		assertEquals(90, student1.getLabsAverage());
	}

	@Test
	void testResultNeedsRounded() {
		Student student1 = new Student("213", "Tom", "Thomas");
		Grade lab1 = new Grade(10, 9);
		Grade lab2 = new Grade(10, 9);
		Grade lab3 = new Grade(10, 8);
		Grade lab4 = new Grade(10, 7);
		student1.addGradeToLabs(lab1);
		student1.addGradeToLabs(lab2);
		student1.addGradeToLabs(lab3);
		student1.addGradeToLabs(lab4);

		assertEquals(83, student1.getLabsAverage());
	}

}
