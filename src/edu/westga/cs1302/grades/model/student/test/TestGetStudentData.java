package edu.westga.cs1302.grades.model.student.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.grades.model.Grade;
import edu.westga.cs1302.grades.model.Student;

/**
 * Test getting the student's data
 * 
 * @author jeremy.trimble
 * @version 7/22/2018
 */
public class TestGetStudentData {

	/**
	 * Test getting the student's data
	 */
	@Test
	public void testGetStudentData() {
		Grade lab1 = new Grade(10, 10);
		Grade lab2 = new Grade(10, 10);
		Grade project1 = new Grade(100, 81);
		Grade project2 = new Grade(100, 90);
		Grade test1 = new Grade(100, 100);
		Grade test2 = new Grade(100, 92);
		Student student1 = new Student("1254", "Tom", "James");
		student1.addGradeToLabs(lab1);
		student1.addGradeToLabs(lab2);
		student1.addGradeToProjects(project1);
		student1.addGradeToProjects(project2);
		student1.addGradeToTests(test1);
		student1.addGradeToTests(test2);

		assertEquals("1254,Tom,James,10,10,,81,90,,100,92", student1.getStudentData());
	}

}
