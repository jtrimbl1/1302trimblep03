package edu.westga.cs1302.grades.model;

import java.util.ArrayList;

/**
 * Manages a student object
 * 
 * @author jeremy.trimble
 * @version 7/16/2018
 */
public class Student {
	private String firstName;
	private String lastName;
	private String idNumber;
	private ArrayList<Grade> labs;

	private ArrayList<Grade> tests;
	private ArrayList<Grade> projects;

	/**
	 * Creates a new Student object
	 * 
	 * @precondition: firstName != null && !firstName.equals("") lastName != null &&
	 *                !lastName.equals("") && idNumber > 0
	 * 
	 * @param firstName
	 *            The student's first Name
	 * @param lastName
	 *            The student's last name
	 * @param idNumber
	 *            The student's ID Number
	 * 
	 */
	public Student(String idNumber, String firstName, String lastName) {
		if (firstName == null || firstName.equals("")) {
			throw new IllegalArgumentException("First Name must not be null or empty");
		}
		if (lastName == null || lastName.equals("")) {
			throw new IllegalArgumentException("Last Name must not be null or empty");
		}
		if (idNumber == null) {
			throw new IllegalArgumentException("ID number must not be null");
		}

		this.firstName = firstName;
		this.lastName = lastName;
		this.idNumber = idNumber;
		this.labs = new ArrayList<Grade>();
		this.tests = new ArrayList<Grade>();
		this.projects = new ArrayList<Grade>();

	}

	/**
	 * Overrides the toString method
	 * 
	 * @precondition: none
	 * 
	 * @return String describing the student
	 */
	public String toString() {
		return this.idNumber + " is the ID Number for " + this.firstName + " " + this.lastName + ".";
	}

	/**
	 * List all the student's data
	 * 
	 * @precondition none
	 * @return string single line string of the student's data
	 * 
	 */
	public String getStudentData() {
		String returnString = this.idNumber + "," + this.firstName + "," + this.lastName;

		for (Grade lab : this.labs) {
			returnString += "," + lab.getPointsEarned();
		}
		returnString += ",";
		for (Grade project : this.projects) {
			returnString += "," + project.getPointsEarned();
		}
		returnString += ",";
		for (Grade test : this.tests) {
			returnString += "," + test.getPointsEarned();
		}

		return returnString;
	}

	/**
	 * Adds grades to labs Arraylist
	 * 
	 * @precondition none
	 * 
	 * @postcondition this.labs = this.labs.size()@prev + 1
	 * 
	 * @param lab
	 *            The lab to be added to the ArrayList
	 */
	public void addGradeToLabs(Grade lab) {
		this.labs.add(lab);
	}

	/**
	 * Adds grades to test Arraylist
	 * 
	 * @precondition none
	 * 
	 * @postcondition this.tests = this.tests.size()@prev + 1
	 * 
	 * @param test
	 *            The test to be added to the ArrayList
	 */
	public void addGradeToTests(Grade test) {
		this.tests.add(test);
	}

	/**
	 * Adds grades to projects Arraylist
	 * 
	 * @precondition none
	 * 
	 * @postcondition this.projects = this.projects.size()@prev + 1
	 * 
	 * @param project
	 *            The project to be added to the ArrayList
	 */
	public void addGradeToProjects(Grade project) {
		this.projects.add(project);
	}

	/**
	 * Returns the lab grades
	 * 
	 * @precondition none
	 * @return the int of grades
	 * 
	 */
	public int countOfLabs() {
		return this.labs.size();
	}

	/**
	 * Returns the test grades
	 * 
	 * @precondition none
	 * @return the int of test grades
	 * 
	 */
	public int countOfTests() {
		return this.tests.size();
	}

	/**
	 * Returns the project grades
	 * 
	 * @precondition none
	 * @return the int of project grades
	 * 
	 */
	public int countOfProjects() {
		return this.projects.size();
	}

	/**
	 * Returns the students name
	 * 
	 * @precondition none
	 * @return the student's name
	 */
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	/**
	 * Gets the average of the student's labs
	 * 
	 * @precondition none
	 * @return the average of the student's labs grade
	 */
	public int getLabsAverage() {
		double count = 0;
		double sum = 0;
		for (Grade current : this.labs) {
			count++;
			sum += current.getScore();
		}
		int average = (int) ((sum / count) + 0.5);
		return average;
	}

	/**
	 * Gets the average of the student's tests
	 * 
	 * @precondition none
	 * @return the average of the student's test grades
	 */
	public int getTestsAverage() {
		double count = 0;
		double sum = 0;
		for (Grade current : this.tests) {
			count++;
			sum += current.getScore();
		}
		int average = (int) ((sum / count) + 0.5);
		return average;
	}

	/**
	 * Gets the average of the student's projects
	 * 
	 * @precondition none
	 * @return the average of the student's project grades
	 */
	public int getProjectsAverage() {
		double count = 0;
		double sum = 0;
		for (Grade current : this.projects) {
			count++;
			sum += current.getScore();
		}
		int average = (int) ((sum / count) + 0.5);
		return average;
	}

	/**
	 * Returns the overall average of labs, projects, and tests for the student
	 * 
	 * @precondition none
	 * 
	 * @return the overall average of all grades for the student
	 */
	public int getOverallAverage() {
		ArrayList<Grade> allGrades = new ArrayList<Grade>();
		allGrades.addAll(this.projects);
		allGrades.addAll(this.tests);
		allGrades.addAll(this.labs);

		double count = 0.0;
		double sum = 0.0;

		for (Grade current : allGrades) {
			count++;
			sum += current.getScore();
		}

		return (int) ((sum / count) + 0.5);
	}

	/**
	 * Gets the weighted Overall Average for the student
	 * 
	 * @precondition none
	 * 
	 * @return the weighted average of the student's overall grades.
	 */
	public int getWeightedAverage() {
		double labs = getLabsAverage() * 0.2;
		double projects = getProjectsAverage() * 0.35;
		double tests = getTestsAverage() * 0.45;

		return (int) (labs + projects + tests + 0.5);
	}

	/**
	 * Add point to Grade
	 * 
	 * @precondition this.tests.size()>=3
	 * @postcondition grade is 1 point higher
	 */
	public void addPointToTestThree() {
		if (this.tests.size() < 3) {
			throw new IllegalArgumentException("Must have at least 3 test");
		}
		this.tests.get(2).addPoint();
	}

	/**
	 * Returns the labs for the student
	 * 
	 * @precondition none
	 * @return the labs of the student
	 */
	public ArrayList<Grade> getLabs() {
		return this.labs;
	}

	/**
	 * Returns the projects for the student
	 * 
	 * @precondition none
	 * @return the projects of the student
	 */
	public ArrayList<Grade> getProjects() {
		return this.projects;
	}

	/**
	 * Returns the tests for the student
	 * 
	 * @precondition none
	 * @return the tests of the student
	 */
	public ArrayList<Grade> getTests() {
		return this.tests;
	}
}
