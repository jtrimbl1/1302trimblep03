package edu.westga.cs1302.grades.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javafx.scene.control.Alert;

/**
 * Manages the GradeBook Object
 * 
 * @author jeremy.trimble
 * @version 7/19/2018
 * 
 */
public class GradeBook {
	private File gradesTxtFile;
	private Scanner fileScan;
	private ArrayList<Student> students;
	private HashMap<String, Student> studentsMap;

	/**
	 * Creates a new GradeBookObject
	 * 
	 * @precondition
	 * 
	 * @postcondition new GradeBook is created
	 * 
	 * @param file
	 *            The file that contains the grades
	 */
	public GradeBook(File file) {
		this.gradesTxtFile = file;
		this.students = new ArrayList<Student>();
		this.studentsMap = new HashMap<String, Student>();

	}

	/**
	 * Overrides the toString
	 * 
	 * @precondition none
	 * @return string describing the GradeBook
	 */
	public String toString() {
		return "This gradebook holds " + this.students.size() + " students.";
	}

	/**
	 * Creates student objects by reading file
	 * 
	 * @precondition none
	 * @postcondition new student object(s) created
	 */
	public void loadStudentsFromFile() {
		try {
			this.fileScan = new Scanner(this.gradesTxtFile);
			this.fileScan.nextLine();

			while (this.fileScan.hasNextLine()) {
				String currentLine = this.fileScan.nextLine();
				String[] lineValues = currentLine.split(",");
				String id = lineValues[0];
				String firstName = lineValues[1];
				String lastName = lineValues[2];
				Student newStudent = new Student(id, firstName, lastName);
				this.students.add(newStudent);
				this.studentsMap.put(id, newStudent);
			}
			this.fileScan.close();
		} catch (FileNotFoundException fnfe) {
			throwFileNotFoundAlert();

		} catch (NoSuchElementException nsee) {
			throwNoSuchElementAlert();
		}

	}

	/**
	 * Loads all the grades for all the students
	 * 
	 * @precondition none
	 * @postcondition All students grades are loaded
	 * 
	 */
	public void loadAllGrades() {
		addAllLabs();
		addAllProjects();
		addAllTests();
	}

	/**
	 * Adds all lab grades for student in labs arraylist
	 * 
	 * @precondition none
	 * @postcondition student's labs arraylist has all labs.
	 */
	public void addAllLabs() {
		try {
			String[] firstLineValues = readLineOne();
			int studentIndex = 0;
			while (this.fileScan.hasNextLine()) {
				String currentLine = this.fileScan.nextLine();
				String[] lineValues = currentLine.split(",");
				int firstBreak = firstEmptyString(lineValues);

				for (int index = 3; index < firstBreak; index++) {
					int newMaxScore = Integer.parseInt(firstLineValues[index]);
					int newScore = Integer.parseInt(lineValues[index]);
					Grade lab = new Grade(newMaxScore, newScore);
					this.students.get(studentIndex).addGradeToLabs(lab);
				}
				studentIndex++;
			}
			this.fileScan.close();
		} catch (FileNotFoundException fnfe) {
			throwFileNotFoundAlert();

		} catch (NoSuchElementException nsee) {
			throwNoSuchElementAlert();
		} catch (NumberFormatException nfe) {
			throwNumberFormatAlert();
		}

	}

	private String[] readLineOne() throws FileNotFoundException {
		this.fileScan = new Scanner(this.gradesTxtFile);
		String firstLine = this.fileScan.nextLine();
		String[] firstLineValues = firstLine.split(",");
		return firstLineValues;
	}

	/**
	 * Adds all test grades for student in tests arraylist
	 * 
	 * @precondition none
	 * @postcondition student's labs arraylist has all labs.
	 */
	public void addAllProjects() {
		try {
			String[] firstLineValues = readLineOne();
			int studentIndex = 0;
			while (this.fileScan.hasNextLine()) {
				String currentLine = this.fileScan.nextLine();
				String[] lineValues = currentLine.split(",");

				int startOfTests = this.firstEmptyString(lineValues) + 1;
				int endOfTests = secondEmptyString(lineValues);

				for (int index = startOfTests; index < endOfTests; index++) {
					int newMaxScore = Integer.parseInt(firstLineValues[index]);
					int newScore = Integer.parseInt(lineValues[index]);
					Grade project = new Grade(newMaxScore, newScore);
					this.students.get(studentIndex).addGradeToProjects(project);
				}
				studentIndex++;
			}
			this.fileScan.close();
		} catch (FileNotFoundException fnfe) {
			throwFileNotFoundAlert();

		} catch (NoSuchElementException nsee) {
			throwNoSuchElementAlert();
		} catch (NumberFormatException nfe) {
			throwNumberFormatAlert();
		}

	}

	/**
	 * Adds all project grades for student in projects arraylist
	 * 
	 * @precondition none
	 * @postcondition student's projects arraylist has all projects.
	 */
	public void addAllTests() {
		try {
			String[] firstLineValues = readLineOne();
			int studentIndex = 0;
			while (this.fileScan.hasNextLine()) {
				String currentLine = this.fileScan.nextLine();
				String[] lineValues = currentLine.split(",");

				int startOfProjects = this.secondEmptyString(lineValues) + 1;

				for (int index = startOfProjects; index < lineValues.length; index++) {
					int newMaxScore = Integer.parseInt(firstLineValues[index]);
					int newScore = Integer.parseInt(lineValues[index]);
					Grade test = new Grade(newMaxScore, newScore);
					this.students.get(studentIndex).addGradeToTests(test);
				}
				studentIndex++;
			}

			this.fileScan.close();
		} catch (FileNotFoundException fnfe) {
			throwFileNotFoundAlert();

		} catch (NoSuchElementException nsee) {
			throwNoSuchElementAlert();
		} catch (NumberFormatException nfe) {
			throwNumberFormatAlert();
		}

	}

	/**
	 * Finds the first empty string on a line in the file
	 * 
	 * @precondition none
	 * @return the index of the empty string in the array
	 */
	private int firstEmptyString(String[] lineValues) {
		for (int index = 0; index < lineValues.length; index++) {
			if (lineValues[index].equals("")) {
				return index;
			}
		}

		return 0;
	}

	/**
	 * Finds the second empty string on a line in the file
	 * 
	 * @precondition none
	 * @return the index of the empty string in the array
	 */
	private int secondEmptyString(String[] lineValues) {
		int startSearch = this.firstEmptyString(lineValues) + 1;

		for (int index = startSearch; index < lineValues.length; index++) {
			if (lineValues[index].equals("")) {
				return index;
			}
		}

		return 0;
	}

	/**
	 * Returns the students arrayList
	 * 
	 * @precondition: none
	 * @return the student arraylist
	 */
	public ArrayList<Student> getStudents() {
		return this.students;
	}

	private void throwNoSuchElementAlert() {
		Alert noSuchElement = new Alert(Alert.AlertType.ERROR);
		noSuchElement.setTitle("No Such Element.");
		noSuchElement.setContentText("The is no such element");
		noSuchElement.showAndWait();
	}

	private void throwFileNotFoundAlert() {
		Alert fileNotFound = new Alert(Alert.AlertType.ERROR);
		fileNotFound.setTitle("File not found.");
		fileNotFound.setContentText("The file entered is was not found");
		fileNotFound.showAndWait();
	}

	private void throwNumberFormatAlert() {
		Alert numberFormat = new Alert(Alert.AlertType.ERROR);
		numberFormat.setTitle("Must be in number format");
		numberFormat.setContentText(
				"String not parsable as integer.\nMay not be able to read number from file\nor a grade may be missing for a student.");
		numberFormat.showAndWait();

	}

	/**
	 * Returns the student value of the provided hashMap key
	 * 
	 * @param id
	 *            Id to be searched for
	 * @return the student with the specified key
	 */
	public Student getStudent(String id) {
		for (String current : this.studentsMap.keySet()) {
			if (current.equals(id)) {
				return this.studentsMap.get(current);
			}
		}
		return null;
	}

	/**
	 * Add a point to test03 for each student
	 * 
	 * @precondition none
	 * @postcondition each student's test03 grade is one point higher
	 */
	public void addPointToTestThreeEachStudent() {
		for (Student current : this.students) {
			current.addPointToTestThree();
		}
	}

	/**
	 * String of just the current Student plus header
	 * 
	 * @precondition none
	 * @return a string of the header row and current student
	 * @param student
	 *            The student whom grades are to be returned
	 */
	public String currentStudentsGrades(Student student) {
		String returnString = getHeaderRow() + System.lineSeparator() + student.getStudentData();

		return returnString;
	}

	/**
	 * String of grades for all students
	 * 
	 * @precondition none
	 * @return string of all students' grades
	 */
	public String allStudentsGrades() {
		String returnString = getHeaderRow();

		for (Student current : this.students) {
			returnString += System.lineSeparator() + current.getStudentData();
		}

		return returnString;
	}

	private String getHeaderRow() {
		String returnString = "ID,First Name,Last Name";
		Student tempStudent = this.getStudents().get(0);

		for (Grade lab : tempStudent.getLabs()) {
			returnString += "," + lab.getMaxPoints();
		}
		returnString += ",";
		for (Grade project : tempStudent.getProjects()) {
			returnString += "," + project.getMaxPoints();
		}
		returnString += ",";
		for (Grade test : tempStudent.getTests()) {
			returnString += "," + test.getMaxPoints();
		}

		return returnString;
	}
}
