package edu.westga.cs1302.grades.controller;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * The entry point for the application
 * 
 * @author jeremy.trimble
 * @version 7/16/2018
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("../view/GradeBookView.fxml"));
			Pane thePane = loader.load();
			Scene theScene = new Scene(thePane);
			primaryStage.setScene(theScene);
			primaryStage.show();
		} catch (IOException ioe) {
			Alert message = new Alert(AlertType.ERROR);
			message.setTitle("GradeBook by Jeremy Trimble");
			message.setContentText(ioe.getMessage());
			System.out.println(ioe.getMessage());
			message.showAndWait();
		}

	}

	/**
	 * Call start method and starts the application.
	 * 
	 * @precondition: none
	 * @postcondition: the application is running
	 * 
	 * @param args
	 *            Not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
